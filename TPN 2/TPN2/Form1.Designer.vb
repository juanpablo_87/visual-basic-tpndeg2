﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtporcentaje1 = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtmayo1 = New System.Windows.Forms.TextBox()
        Me.txtabril1 = New System.Windows.Forms.TextBox()
        Me.txtcodigo1 = New System.Windows.Forms.TextBox()
        Me.txtnombre1 = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtporcentaje2 = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtmayo2 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtabril2 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtcodigo2 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtnombre2 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtporcentaje3 = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtmayo3 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtabril3 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtcodigo3 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtnombre3 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.btncalcular = New System.Windows.Forms.Button()
        Me.btnmostrar = New System.Windows.Forms.Button()
        Me.txtporcentajetotal = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.btnnuevo = New System.Windows.Forms.Button()
        Me.btnsalir = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtporcentaje1)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.txtmayo1)
        Me.GroupBox1.Controls.Add(Me.txtabril1)
        Me.GroupBox1.Controls.Add(Me.txtcodigo1)
        Me.GroupBox1.Controls.Add(Me.txtnombre1)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(14, 5)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(204, 269)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Empresa 1"
        '
        'txtporcentaje1
        '
        Me.txtporcentaje1.BackColor = System.Drawing.Color.White
        Me.txtporcentaje1.Location = New System.Drawing.Point(80, 196)
        Me.txtporcentaje1.Name = "txtporcentaje1"
        Me.txtporcentaje1.ReadOnly = True
        Me.txtporcentaje1.Size = New System.Drawing.Size(116, 20)
        Me.txtporcentaje1.TabIndex = 9
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(7, 203)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(68, 13)
        Me.Label13.TabIndex = 8
        Me.Label13.Text = "Porcentaje"
        '
        'txtmayo1
        '
        Me.txtmayo1.Location = New System.Drawing.Point(80, 135)
        Me.txtmayo1.Name = "txtmayo1"
        Me.txtmayo1.Size = New System.Drawing.Size(116, 20)
        Me.txtmayo1.TabIndex = 7
        '
        'txtabril1
        '
        Me.txtabril1.Location = New System.Drawing.Point(80, 99)
        Me.txtabril1.Name = "txtabril1"
        Me.txtabril1.Size = New System.Drawing.Size(116, 20)
        Me.txtabril1.TabIndex = 6
        '
        'txtcodigo1
        '
        Me.txtcodigo1.Location = New System.Drawing.Point(80, 63)
        Me.txtcodigo1.Name = "txtcodigo1"
        Me.txtcodigo1.Size = New System.Drawing.Size(116, 20)
        Me.txtcodigo1.TabIndex = 5
        '
        'txtnombre1
        '
        Me.txtnombre1.Location = New System.Drawing.Point(80, 26)
        Me.txtnombre1.Name = "txtnombre1"
        Me.txtnombre1.Size = New System.Drawing.Size(116, 20)
        Me.txtnombre1.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 142)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(37, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Mayo"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 99)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(32, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Abril"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(7, 63)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Codigo"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nombre"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtporcentaje2)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.txtmayo2)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.txtabril2)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.txtcodigo2)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.txtnombre2)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Location = New System.Drawing.Point(218, 5)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(220, 269)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Empresa 2"
        '
        'txtporcentaje2
        '
        Me.txtporcentaje2.BackColor = System.Drawing.Color.White
        Me.txtporcentaje2.Location = New System.Drawing.Point(80, 196)
        Me.txtporcentaje2.Name = "txtporcentaje2"
        Me.txtporcentaje2.ReadOnly = True
        Me.txtporcentaje2.Size = New System.Drawing.Size(116, 20)
        Me.txtporcentaje2.TabIndex = 17
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(7, 203)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(68, 13)
        Me.Label14.TabIndex = 16
        Me.Label14.Text = "Porcentaje"
        '
        'txtmayo2
        '
        Me.txtmayo2.Location = New System.Drawing.Point(80, 135)
        Me.txtmayo2.Name = "txtmayo2"
        Me.txtmayo2.Size = New System.Drawing.Size(116, 20)
        Me.txtmayo2.TabIndex = 15
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(7, 142)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(37, 13)
        Me.Label8.TabIndex = 14
        Me.Label8.Text = "Mayo"
        '
        'txtabril2
        '
        Me.txtabril2.Location = New System.Drawing.Point(80, 95)
        Me.txtabril2.Name = "txtabril2"
        Me.txtabril2.Size = New System.Drawing.Size(116, 20)
        Me.txtabril2.TabIndex = 13
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(7, 102)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(32, 13)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Abril"
        '
        'txtcodigo2
        '
        Me.txtcodigo2.Location = New System.Drawing.Point(80, 59)
        Me.txtcodigo2.Name = "txtcodigo2"
        Me.txtcodigo2.Size = New System.Drawing.Size(116, 20)
        Me.txtcodigo2.TabIndex = 11
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(7, 66)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(45, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "codigo"
        '
        'txtnombre2
        '
        Me.txtnombre2.Location = New System.Drawing.Point(80, 26)
        Me.txtnombre2.Name = "txtnombre2"
        Me.txtnombre2.Size = New System.Drawing.Size(116, 20)
        Me.txtnombre2.TabIndex = 9
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 33)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(50, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Nombre"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtporcentaje3)
        Me.GroupBox3.Controls.Add(Me.Label15)
        Me.GroupBox3.Controls.Add(Me.txtmayo3)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.txtabril3)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.txtcodigo3)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.txtnombre3)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Location = New System.Drawing.Point(437, 5)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(223, 269)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Empresa 3"
        '
        'txtporcentaje3
        '
        Me.txtporcentaje3.BackColor = System.Drawing.Color.White
        Me.txtporcentaje3.Location = New System.Drawing.Point(87, 196)
        Me.txtporcentaje3.Name = "txtporcentaje3"
        Me.txtporcentaje3.ReadOnly = True
        Me.txtporcentaje3.Size = New System.Drawing.Size(116, 20)
        Me.txtporcentaje3.TabIndex = 17
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(14, 203)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(68, 13)
        Me.Label15.TabIndex = 16
        Me.Label15.Text = "Porcentaje"
        '
        'txtmayo3
        '
        Me.txtmayo3.Location = New System.Drawing.Point(80, 135)
        Me.txtmayo3.Name = "txtmayo3"
        Me.txtmayo3.Size = New System.Drawing.Size(116, 20)
        Me.txtmayo3.TabIndex = 15
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(7, 142)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(37, 13)
        Me.Label12.TabIndex = 14
        Me.Label12.Text = "Mayo"
        '
        'txtabril3
        '
        Me.txtabril3.Location = New System.Drawing.Point(80, 99)
        Me.txtabril3.Name = "txtabril3"
        Me.txtabril3.Size = New System.Drawing.Size(116, 20)
        Me.txtabril3.TabIndex = 13
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(7, 106)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(32, 13)
        Me.Label11.TabIndex = 12
        Me.Label11.Text = "Abril"
        '
        'txtcodigo3
        '
        Me.txtcodigo3.Location = New System.Drawing.Point(80, 63)
        Me.txtcodigo3.Name = "txtcodigo3"
        Me.txtcodigo3.Size = New System.Drawing.Size(116, 20)
        Me.txtcodigo3.TabIndex = 11
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(7, 70)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(46, 13)
        Me.Label10.TabIndex = 10
        Me.Label10.Text = "Codigo"
        '
        'txtnombre3
        '
        Me.txtnombre3.Location = New System.Drawing.Point(80, 26)
        Me.txtnombre3.Name = "txtnombre3"
        Me.txtnombre3.Size = New System.Drawing.Size(116, 20)
        Me.txtnombre3.TabIndex = 9
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(7, 33)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(50, 13)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Nombre"
        '
        'btncalcular
        '
        Me.btncalcular.Location = New System.Drawing.Point(457, 303)
        Me.btncalcular.Name = "btncalcular"
        Me.btncalcular.Size = New System.Drawing.Size(73, 23)
        Me.btncalcular.TabIndex = 3
        Me.btncalcular.Text = "Calcular"
        Me.btncalcular.UseVisualStyleBackColor = True
        '
        'btnmostrar
        '
        Me.btnmostrar.Location = New System.Drawing.Point(539, 303)
        Me.btnmostrar.Name = "btnmostrar"
        Me.btnmostrar.Size = New System.Drawing.Size(66, 23)
        Me.btnmostrar.TabIndex = 4
        Me.btnmostrar.Text = "Mostrar"
        Me.btnmostrar.UseVisualStyleBackColor = True
        '
        'txtporcentajetotal
        '
        Me.txtporcentajetotal.BackColor = System.Drawing.Color.White
        Me.txtporcentajetotal.Location = New System.Drawing.Point(127, 304)
        Me.txtporcentajetotal.Name = "txtporcentajetotal"
        Me.txtporcentajetotal.ReadOnly = True
        Me.txtporcentajetotal.Size = New System.Drawing.Size(116, 20)
        Me.txtporcentajetotal.TabIndex = 9
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(21, 309)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(101, 13)
        Me.Label16.TabIndex = 8
        Me.Label16.Text = "Porcentaje Total"
        '
        'btnnuevo
        '
        Me.btnnuevo.Location = New System.Drawing.Point(381, 303)
        Me.btnnuevo.Name = "btnnuevo"
        Me.btnnuevo.Size = New System.Drawing.Size(69, 23)
        Me.btnnuevo.TabIndex = 10
        Me.btnnuevo.Text = "Nuevo"
        Me.btnnuevo.UseVisualStyleBackColor = True
        '
        'btnsalir
        '
        Me.btnsalir.Location = New System.Drawing.Point(303, 303)
        Me.btnsalir.Name = "btnsalir"
        Me.btnsalir.Size = New System.Drawing.Size(71, 23)
        Me.btnsalir.TabIndex = 11
        Me.btnsalir.Text = "Salir"
        Me.btnsalir.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(677, 337)
        Me.Controls.Add(Me.btnsalir)
        Me.Controls.Add(Me.btnnuevo)
        Me.Controls.Add(Me.txtporcentajetotal)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.btnmostrar)
        Me.Controls.Add(Me.btncalcular)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "Form1"
        Me.Text = "Formulario de Empresas"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtmayo1 As TextBox
    Friend WithEvents txtabril1 As TextBox
    Friend WithEvents txtcodigo1 As TextBox
    Friend WithEvents txtnombre1 As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtmayo2 As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txtabril2 As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txtcodigo2 As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtnombre2 As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtmayo3 As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents txtabril3 As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txtcodigo3 As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txtnombre3 As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txtporcentaje1 As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents txtporcentaje2 As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents txtporcentaje3 As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents btncalcular As Button
    Friend WithEvents btnmostrar As Button
    Friend WithEvents txtporcentajetotal As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents btnnuevo As Button
    Friend WithEvents btnsalir As Button
End Class
